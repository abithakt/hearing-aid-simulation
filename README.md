# hearing-aid-simulation

This project was created for the course Signals & Systems, with code contributed
by Jayasri Vaidyaraman, Narmatha K, and S Shruthi.

## How it works

* The program reads the input sound signal.
* The input signal is passed through four filters, separating it into four frequency bands.
* The maximum power of each band is calculated using the Fast Fourier Transform.
* Accordingly, selective amplification is done for each band.
* Finally, the separate bands are put back together and the processed signal
suitable for a hearing aid is obtained.

## License

Copyright 2018 Abitha K Thyagarajan, Jayasri Vaidyaraman, Narmatha K, S Shruthi    

<blockquote>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use 
this file except in compliance with the License. You may obtain a copy of the 
License at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0).

Unless required by applicable law or agreed to in writing, software distributed 
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
CONDITIONS OF ANY KIND, either express or implied. See the License for the 
specific language governing permissions and limitations under the License.
</blockquote>