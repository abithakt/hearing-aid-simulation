[f,fs] = audioread('in.wav');
%[f,fs] = audioread('out1.wav');

% lowpass filter -- 0 to 300 Hz
[b, a] = butter(6, 300/(fs/2));
band1 = filter(b, a, f);

% bandpass filter -- 300 to 3000 Hz

[b,a] = butter(6,[300 3000]/(fs/2), 'bandpass');
band2 = filter(b,a,f);

% bandpass filter -- 3000 to 8000 Hz

[b,a] = butter(6,[3000 8000]/(fs/2), 'bandpass');
band3 = filter(b,a,f);

% bandpass filter -- 8000 to 20000 Hz

[b,a] = butter(6,[8000 20000]/(fs/2), 'bandpass');
band4 = filter(b,a,f);

% determining the power in each band

ff1=fft(band1);
ff2=fft(band2);
ff3=fft(band3);
ff4=fft(band4);

h1=sum(abs(ff1));
h2=sum(abs(ff2));
h3=sum(abs(ff3));
h4=sum(abs(ff4));

band1=band1*h1(1);
band2=band2*h2(1);
band3=band3*h3(1);
band4=band4*h4(1);

% amplifying
[m, n] = size(band1);
for i = 1:m
    band1(m, 1) = band1(m, 1) * h1(1);
    band1(m, 2) = band1(m, 2) * h1(2);
end
[m, n] = size(band2);
for i = 1:m
    band2(m, 1) = band2(m, 1) * h2(1);
    band2(m, 2) = band2(m, 2) * h2(2);
end
[m, n] = size(band3);
for i = 1:m
    band3(m, 1) = band3(m, 1) * h3(1);
    band3(m, 2) = band3(m, 2) * h3(2);
end
[m, n] = size(band4);
for i = 1:m
    band4(m, 1) = band4(m, 1) * h4(1);
    band4(m, 2) = band4(m, 2) * h4(2);
end

band1 = 0.3*band1; % to reduce noise

% summing up the four bands
f1=band1+band2+band3+band4;

audiowrite('out1.wav',f1,fs);
